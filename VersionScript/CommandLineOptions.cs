﻿using CommandLine;
using CommandLine.Text;
using System;
using System.Collections.Generic;

namespace VersionScript
{
	public class CommandLineOptions
	{
		[Option('i', "input", Required = true, HelpText = "Specifies a list of files or directories")]
		public IEnumerable<string> Inputs { get; set; }

		[Option('n', "new-version", Required = true, HelpText = "Specifies the version to set in all files (e.g. 1.0.0, v2.3.6, myapp_v2.0.0.0)")]
		public string NewVersion { get; set; }

		[Option('c', "commit", Required = true, HelpText = "Specifies the commit version or hash (e.g. b5538059a2e3e9d5d70aedd0586e07a3bc9110e1)")]
		public string Commit { get; set; }

		[Option('d', "date", Default = true, HelpText = "Indicates if the current date is appended to the detailled version")]
		public bool IncludeDate { get; set; }

		[Option("verbose", Default = false, HelpText = "Show verbose output")]
		public bool Verbose {  get; set; }

		[Option("dryrun", Default = false, HelpText = "Only show what actions will be taken, don't replace anything")]
		public bool DryRun { get; set; }
	}
}
