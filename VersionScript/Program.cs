﻿using CommandLine;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using VersionScript.FileTypes;

namespace VersionScript
{
	class Program
	{
		static void Main(string[] args)
		{
			Parser.Default.ParseArguments<CommandLineOptions>(args)
				   .WithParsed<CommandLineOptions>(o =>
				   {
					   var p = new Program();

					   if (o.DryRun)
						   Console.WriteLine("Dry run, only detecting files and printing current versions");

					   var versionInfo = new VersionInfo(o.NewVersion, o.Commit, o.IncludeDate);
					   if (o.Verbose)
						   Console.WriteLine($"Changing file versions to {versionInfo.Dotted4}");

					   var files = p.EnumerateFiles(o.Inputs, o.Verbose).ToList();

					   foreach (var file in files)
					   {
						   var oldVersion = file.ReadVersion();
						   if (o.DryRun)
						   {
								Console.WriteLine($"{file.FileName}: {oldVersion}");
						   }
						   else
						   {
							   file.SetVersion(versionInfo);
							   Console.WriteLine($"{file.FileName}: {oldVersion} -> {file.ReadVersion()}");
						   }
					   }
				   });

			if (System.Diagnostics.Debugger.IsAttached)
			{
				Console.WriteLine("Press any key to continue ...");
				Console.Read();
			}
		}

		private IEnumerable<IFileWithVersion> EnumerateFiles(IEnumerable<string> inputs, bool verbose)
		{
			foreach (var input in inputs)
			{
				// check if the file is a directory
				if (Directory.Exists(input))
				{
					if (verbose)
						Console.WriteLine($"Scanning directory {input}");

					foreach (var file in Directory.EnumerateFiles(input, "*.*", SearchOption.AllDirectories))
					{
						var check = CheckIfInterestingFile(file);
						if (check != null && check.CanReadVersion)
						{
							yield return check;
							if (verbose)
								Console.WriteLine($"Found file {check.FileName}");
						}
					}
				}

				// check if the input file exists
				else if (File.Exists(input))
				{
					var check = CheckIfInterestingFile(input);
					if (check != null && check.CanReadVersion)
					{
						yield return check;
						if (verbose)
							Console.WriteLine($"Found file {check.FileName}");
					}
					else
					{
						Console.WriteLine($"Warning: unknown file type: '{input}'");
					}
				}

				// else invalid input
				else
				{
					Console.WriteLine($"Warning: input file or directory does not exist: '{input}'");
				}
			}
		}

		private IFileWithVersion CheckIfInterestingFile(string filename)
		{
			switch (Path.GetExtension(filename).ToLower())
			{
				case ".csproj":
					return new CsProjectFile(filename);
				case ".cs":
					if (Path.GetFileName(filename).ToLower() == "assemblyinfo.cs")
						return new AssemblyInfoFile(filename);
					break;
			}

			return null;
		}
	}
}