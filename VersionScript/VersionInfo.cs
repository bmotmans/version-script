﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace VersionScript
{
    public class VersionInfo
    {
        public VersionInfo(string rawVersion, string commit, bool includeDate)
        {
            RawVersion = rawVersion;
            Commit = commit;
            IncludeDate = includeDate;
            var regex = new Regex("((\\d+)(?:\\.))((\\d+)(?:\\.))?((\\d+)(?:\\.))?((\\d+)(?:$|.))?", RegexOptions.Singleline);
            var m = regex.Match(RawVersion);

            if (!m.Success || m.Groups.Count != 9)
                throw new ArgumentException($"Failed to parse version '{rawVersion}'");

            Major = int.Parse(m.Groups[2].Value);


            var values = new List<int>();
            if (m.Groups[4].Length != 0)
            {
                values.Add(int.Parse(m.Groups[4].Value));
            }
            if (m.Groups[6].Length != 0)
            {
                values.Add(int.Parse(m.Groups[6].Value));
            }
            if (m.Groups[8].Length != 0)
            {
                values.Add(int.Parse(m.Groups[8].Value));
            }

            if (values.Count > 0)
                Minor = values[0];
            if (values.Count > 1)
                Build = values[1];
            if (values.Count > 2)
                Revision = values[2];

            //TODO: support for wildcard * character
        }

        public string RawVersion { get; }
        public string Commit { get; }
        public bool IncludeDate { get; }

        public string Dotted2 => $"{Major}.{Minor}";

        public string Dotted3 => $"{Major}.{Minor}";

        public string Dotted4 => $"{Major}.{Minor}.{Build}.{Revision}";

        public string FullVersion
        {
            get
            {
                if (string.IsNullOrEmpty(Commit))
                {
                    return Dotted4;
                } else
                {
                    if (IncludeDate)
                    {
                        return $"{Major}.{Minor}.{Build}.{Revision} (commit: {Commit}, timestamp: {DateTime.Now:yyyy-MM-dd HH:mm:ss zzz})";
                    } else
                    {
                        return $"{Major}.{Minor}.{Build}.{Revision} (commit: {Commit})";
                    }
                }
            }
        }

        public int Major { get; }

        public int Minor { get; }

        public int Build { get; }

        public int Revision { get; }
    }
}
