﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VersionScript.FileTypes
{
    public class CppHeaderFile : IFileWithVersion
    {
        internal CppHeaderFile(string filename)
        {
            FileName = filename ?? throw new ArgumentNullException(nameof(filename));
        }

        public string FileName { get; private set; }

        public bool CanReadVersion { get; }

        public string ReadVersion()
        {
            throw new NotImplementedException();
        }

        public void SetVersion(VersionInfo version)
        {
            throw new NotImplementedException();
        }
    }
}
