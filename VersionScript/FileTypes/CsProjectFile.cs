﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace VersionScript.FileTypes
{
    public class CsProjectFile : IFileWithVersion
    {
        private XmlDocument doc;
        private XmlElement propGroup;

        internal CsProjectFile(string filename)
        {
            FileName = filename ?? throw new ArgumentNullException(nameof(filename));

            try
            {
                doc = new XmlDocument();
                doc.Load(FileName);

                if (doc.DocumentElement == null)
                    return;
                if (doc.DocumentElement.Name != "Project")
                    return;
                if (doc.DocumentElement.GetAttribute("Sdk") == null)
                    return;

                foreach (XmlElement elem in doc.DocumentElement.ChildNodes)
                {
                    if (elem.Name == "PropertyGroup" && elem["TargetFramework"] != null)
                    {
                        propGroup = elem;
                        CanReadVersion = true;
                        break;
                    }

                }
            } catch (Exception ex)
            {
                Console.WriteLine($"Failed to parse {filename}: {ex.Message}");
            }
        }

        public string FileName { get; private set; }

        public bool CanReadVersion { get; }

        public string ReadVersion()
        {
            if (!CanReadVersion)
                throw new InvalidOperationException($".csproj file {FileName} does not contain version info");

            return propGroup["AssemblyVersion"]?.InnerText ?? null;
        }

        public void SetVersion(VersionInfo version)
        {
            if (!CanReadVersion)
                throw new InvalidOperationException($".csproj file {FileName} does not contain version info");

            if (propGroup["AssemblyVersion"] == null)
                propGroup.AppendChild(doc.CreateElement("AssemblyVersion"));
            propGroup["AssemblyVersion"].InnerText = version.Dotted4;

            if (propGroup["FileVersion"] == null)
                propGroup.AppendChild(doc.CreateElement("FileVersion"));
            propGroup["FileVersion"].InnerText = version.Dotted4;

            if (propGroup["InformationalVersion"] == null)
                propGroup.AppendChild(doc.CreateElement("InformationalVersion"));
            propGroup["InformationalVersion"].InnerText = version.FullVersion;

            doc.Save(FileName);
        }
    }
}
