﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace VersionScript.FileTypes
{
    /*
     * #if DEBUG
 [assembly: AssemblyConfiguration("Debug")]
 #else
 [assembly: AssemblyConfiguration("Release")]
 #endif
   
    [assembly: AssemblyVersion("")]
    [assembly: AssemblyFileVersion("")]
    [assembly: AssemblyInformationalVersion("")]
    */

    public class AssemblyInfoFile : IFileWithVersion
    {
        private const string patternAssemblyVersion = "AssemblyVersion\\s+\\(\"([\\d\\.\\*]+)\"\\)";
        private const string patternAssemblyFileVersion = "AssemblyFileVersion\\s+\\(\"([\\d\\.\\*]+)\"\\)";
        private const string patternAssemblyInfoVersion = "AssemblyInformationalVersion\\s+\\(\"(.*)\"\\)";

        private readonly Regex regexAssemblyVersion = new Regex(patternAssemblyVersion, RegexOptions.Multiline);

        internal AssemblyInfoFile(string filename)
        {
            FileName = filename ?? throw new ArgumentNullException(nameof(filename));
        }

        public string FileName { get; private set; }

        public bool CanReadVersion => true;

        public string ReadVersion()
        {
            string content = File.ReadAllText(FileName);

            var m = regexAssemblyVersion.Match(content);
            if (m.Success)
            {
                return m.Groups[1].Value;
            }

            return null;
        }

        public void SetVersion(VersionInfo version)
        {
            string content = File.ReadAllText(FileName);

            content = ReplaceAttribute(content, patternAssemblyVersion, "AssemblyVersion", version.Dotted4);
            content = ReplaceAttribute(content, patternAssemblyFileVersion, "AssemblyFileVersion", version.Dotted4);
            content = ReplaceAttribute(content, patternAssemblyInfoVersion, "AssemblyInformationalVersion", version.FullVersion);

            File.WriteAllText(FileName, content);
        }

        private string ReplaceAttribute(string content, string pattern, string attributeName, string version)
        {
            string replacement = $"{attributeName} (\"{version}\")";

            StringBuilder sb = new StringBuilder();

            var m = Regex.Match(content, pattern, RegexOptions.Multiline);
            if (m.Success)
            {
                int lastIndex = 0;
                foreach (Capture c in m.Captures)
                {
                    sb.Append(content.Substring(lastIndex, c.Index));
                    sb.Append(replacement);
                    lastIndex = c.Index + c.Length;
                }

                sb.Append(content.Substring(lastIndex));
            } else
            {
                sb.Append(content);
                sb.AppendLine($"[assembly: {replacement}]");
            }

            return sb.ToString();
        }
    }
}
