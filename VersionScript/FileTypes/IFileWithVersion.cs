﻿using System;

namespace VersionScript.FileTypes
{
    public interface IFileWithVersion
    {
        string FileName { get; }

        bool CanReadVersion { get; }

        string ReadVersion();

        public void SetVersion(VersionInfo version);
    }
}
